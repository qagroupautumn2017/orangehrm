package OrangeHRMApp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.qameta.allure.Step;

public class SingUpPage {

	@FindBy(css = ".formInputText.ac_input")
	private WebElement employeenameField;

	@FindBy(id = "systemUser_userName")
	private WebElement usernameField;

	@FindBy(id = "systemUser_password")
	private WebElement passwordField;

	@FindBy(id = "systemUser_confirmPassword")
	private WebElement confirmPasswordField;

	@FindBy(css = ".addbutton")
	private WebElement addButton;

	private WebDriver driver;

	public SingUpPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@Step("SingUp with employeeName <{employeeName}>, username <{username}>, password <{password}> and confirmpassword <{confirmpassword}>")
	public AddUserPage singUp(String employeename, String username, String password, String confirmpassword) {
		enterEmployeename(employeename);
		enteUsername(username);
		entePassword(password);
		enteConfirmPassword(confirmpassword);
		clickAddButton();
		return new AddUserPage(driver);
	}

	@Step("Enter employee {employeename}")
	public void enterEmployeename(String employeename) {
		employeenameField.sendKeys(employeename);
	}

	@Step("Enter username {username}")
	public void enteUsername(String username) {
		usernameField.sendKeys(username);
	}

	@Step("Enter password {password}")
	public void entePassword(String password) {
		passwordField.sendKeys(password);
	}

	@Step("Enter confirmpassword {confirmpassword}")
	public void enteConfirmPassword(String confirmpassword) {
		confirmPasswordField.sendKeys(confirmpassword);
	}
	@Step("Click 'Add' button")
	public void clickAddButton() {
		addButton.click();
	}

}