package OrangeHRMApp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.qameta.allure.Step;

public class AdminPage {
	@FindBy(css = ".firstLevelMenu:not(#searchform)")
	private WebElement adminTab;
	@FindBy(css = ".arrow:not(#searchform)")
	private WebElement userManagementTab;
	@FindBy(xpath = ".//a[contains(text(),'Users')]")
	private WebElement usersTab;
	@FindBy(name = "btnAdd")
	private WebElement addBtn;

	private WebDriver driver;

	public AdminPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@Step("Add new User")
	public SingUpPage addUser() {
		clickAdminTab();
		clickUserManagementTab();
		clickUsersTab();
		clickAddBtn();
		return new SingUpPage(driver);
	}

	@Step("Click 'Admin' tab")
	public void clickAdminTab() {
		adminTab.click();
	}

	@Step("Click 'UserManagement' tab")
	public void clickUserManagementTab() {
		userManagementTab.click();
	}

	@Step("Click 'Users' tab")
	public void clickUsersTab() {
		usersTab.click();
	}

	@Step("Click 'Add' botton")
	public void clickAddBtn() {
		addBtn.click();
	} 

}
