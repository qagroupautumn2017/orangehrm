package OrangeHRMApp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.qameta.allure.Step;

public class MainPage {
	@FindBy(xpath = ".//b[contains(text(),'Time')]")
	private WebElement timeTab;
	@FindBy(xpath = ".//a[contains(text(),'Attendance')]")
	private WebElement attendanceTab;
	@FindBy(xpath = ".//a[contains(text(),'Employee Records')]")
	private WebElement employeeRecordsTab;

	private WebDriver driver;

	public MainPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@Step("Add new User")
	public TimeTabPage EmployeeRecords() {
		clickTimeTab();
		clickAttendanceTab();
		clickEmployeeRecordsTab();
		
		return new  TimeTabPage(driver);
	}

	@Step("Click 'Time' tab")
	public void clickTimeTab() {
		timeTab.click();
	}

	@Step("Click 'Attendance' tab")
	public void clickAttendanceTab() {
		attendanceTab.click();
	}

	@Step("Click 'EmployeeRecords' tab")
	public void clickEmployeeRecordsTab() {
		employeeRecordsTab.click();
	}

	

}
