package OrangeHRMApp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.qameta.allure.Step;

@SuppressWarnings("deprecation")
public class TimeTabPage {

	private static final String String = null;
	@FindBy(css = ".ac_input")
	private WebElement employeeNameField;
	@FindBy(css = ".ui-datepicker")
	private WebElement datePicker;
	@FindBy(css = ".ui-datepicker-trigger")
	private WebElement datePickerTrigger;
	@FindBy(css = ".ui-state-default")
	private WebElement dayLabel;
	@FindBy(id = "btView")
	private WebElement viewButton;
	@FindBy(xpath = ".//TD[@class='left'][text()='No attendance records to display']")
	private WebElement attendanceRecordTable;
	@FindBy(css = ".punch")
	private WebElement addAttendanceRecordButton;

	private WebDriver driver;
	private String employeeName;

	public TimeTabPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void ViewAttendanceRecord(String employeeName) {
		enterEmployeeName(employeeName);
		openDatePicker();
		clickDayLabel();
		clickViewButton();

	}

	@Step("Enter employeename<{employeeName}>")
	public void enterEmployeeName(String employeeName) {
		employeeNameField.sendKeys(employeeName);
	}

	public DatePicker datePicker() {
		return new DatePicker(datePicker);
	}

	@Step("Open'date Picker'")
	public DatePicker openDatePicker() {
		datePickerTrigger.click();
		return datePicker();
	}

	@Step("Click 'Day'label")
	public void clickDayLabel() {
		dayLabel.click();

	}

	@Step("Click 'View' button")
	public void clickViewButton() {
		viewButton.click();
	}

	public boolean IsTherNoAttendanceRecordsToDisplay() {
		return attendanceRecordTable.isDisplayed();
	}

	public AddAttendanceRecordPage clickaddAttendanceRecordButton() {
		addAttendanceRecordButton.click();
		return new AddAttendanceRecordPage(driver);
	}
}
