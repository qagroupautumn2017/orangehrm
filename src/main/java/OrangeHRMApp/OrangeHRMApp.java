package OrangeHRMApp;

import org.openqa.selenium.WebDriver;

import OrangeHRMTools.Browser;

public class OrangeHRMApp {

	private WebDriver driver;

	public LoginPage openLoginPage() {
		driver = Browser.open();
		driver.get("http://opensource.demo.orangehrmlive.com/");
		return new LoginPage(driver);
	}

	public void close() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}

}
