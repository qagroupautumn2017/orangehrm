package OrangeHRMApp;

import org.openqa.selenium.WebDriver;

import OrangeHRMTools.Browser;

public class OrangeHRMApp2 {

	private WebDriver driver;

	public LoginPage2 openLoginPage() {
		driver = Browser.open();
		driver.get("http://opensource.demo.orangehrmlive.com/");
		return new LoginPage2(driver);
	}

	public void close() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}
}
