package OrangeHRMApp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.qameta.allure.Step;

public class LoginPage {

	@FindBy(name = "txtUsername")
	private WebElement usernameField;

	@FindBy(name = "txtPassword")
	private WebElement passwordField;

	@FindBy(css = ".button")
	private WebElement loginButton;

	private WebDriver driver;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@Step("Login with username <{username}> and password <{password}>")
	public AdminPage loginAs(String username, String password) {
		enterUserName(username);
		enterPassword(password);
		clickLoginButton();
		return new AdminPage(driver);
	}
	public UserPage loginAs1(String username, String password) {
		enterUserName(username);
		enterPassword(password);
		clickLoginButton();
		return new UserPage(driver);
	}

	@Step("Enter username {username}")
	public void enterUserName(String username) {
		usernameField.sendKeys(username);
	}

	@Step("Enter password {password}")
	public void enterPassword(String password) {
		passwordField.sendKeys(password);
	}

	@Step("Click 'Login' button")
	public void clickLoginButton() {
		loginButton.click();
	}

}
