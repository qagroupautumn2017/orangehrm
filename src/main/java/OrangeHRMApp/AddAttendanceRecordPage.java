package OrangeHRMApp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddAttendanceRecordPage {
	@FindBy(css = ".time")
	private WebElement timeField;
	@FindBy(css = ".note")
	private WebElement noteField;
	@FindBy(css = ".punchInbutton")
	private WebElement punchInbutton;
	@FindBy(xpath = ".//TD[contains(text(),'Happy New Year')]")
	private WebElement attendanceRecordTable;
	private WebDriver driver;

	public AddAttendanceRecordPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void PunchIn(String note) {
		enterNote(note);
		clickPunchInbutton();

	}

	public void enterNote(String note) {
		noteField.sendKeys(note);
	}

	public void clickPunchInbutton() {
		punchInbutton.click();
	}

	public boolean IsAttendanceRecordsOnDisplay() {
		return attendanceRecordTable.isDisplayed();

	}

	
	

	
	}

