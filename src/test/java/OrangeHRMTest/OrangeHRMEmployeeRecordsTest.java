package OrangeHRMTest;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import OrangeHRMApp.AddAttendanceRecordPage;
import OrangeHRMApp.AdminPage;

import OrangeHRMApp.LoginPage2;
import OrangeHRMApp.MainPage;
import OrangeHRMApp.OrangeHRMApp;
import OrangeHRMApp.OrangeHRMApp2;
import OrangeHRMApp.TimeTabPage;
import OrangeHRMApp.ViewAttendanceRecordPage;

public class OrangeHRMEmployeeRecordsTest {

	private OrangeHRMApp2 orangeHRMApp2 = new OrangeHRMApp2();

	private WebDriver driver;

	private LoginPage2 loginPage2;

	private AdminPage adminPage;

	private TimeTabPage timeTabPage;

	private MainPage mainPage;
	
	private ViewAttendanceRecordPage viewAttendanceRecordPage;
	
	private AddAttendanceRecordPage addAttendanceRecordPage;

	@Test
	public void OrangeHRMEmployeeRecordsTest() {
		loginPage2 = orangeHRMApp2.openLoginPage();
		mainPage = loginPage2.loginAs2("admin", "admin");
		timeTabPage = mainPage.EmployeeRecords();
		timeTabPage.ViewAttendanceRecord("Jasmine Morgan");
		
		 Assert.assertTrue(timeTabPage.IsTherNoAttendanceRecordsToDisplay());
         
		 addAttendanceRecordPage = timeTabPage.clickaddAttendanceRecordButton();
		 addAttendanceRecordPage.PunchIn("Happy New Year");
		 
		 Assert.assertTrue(addAttendanceRecordPage.IsAttendanceRecordsOnDisplay());
	}
	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		orangeHRMApp2.close();
		
	}
}
