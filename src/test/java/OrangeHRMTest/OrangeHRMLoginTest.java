package OrangeHRMTest;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import OrangeHRMApp.AddUserPage;
import OrangeHRMApp.AdminPage;
import OrangeHRMApp.LoginPage;
import OrangeHRMApp.OrangeHRMApp;
import OrangeHRMApp.SingUpPage;
import OrangeHRMApp.UserPage;

public class OrangeHRMLoginTest {

	private OrangeHRMApp orangeHRMApp = new OrangeHRMApp();

	private WebDriver driver;

	private LoginPage loginPage;

	private AdminPage adminPage;

	private SingUpPage singUpPage;

	private UserPage userPage;

	private AddUserPage addUserPage;

	@Test
	public void OrangeHRMLoginTest() {
		loginPage = orangeHRMApp.openLoginPage();
		adminPage = loginPage.loginAs("admin", "admin");
		singUpPage = adminPage.addUser();
		addUserPage = singUpPage.singUp("Vamsi Ch", "Jasmine", "123456", "123456");
		loginPage = orangeHRMApp.openLoginPage();
		userPage = loginPage.loginAs1("Jasmine", "123456");

		String actualUrl = userPage.getCurrentUrl();
		String expectedUrl = "http://opensource.demo.orangehrmlive.com/index.php/dashboard";

		Assert.assertEquals(actualUrl, expectedUrl, "Incorrect URL");

	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		orangeHRMApp.close();
	}

}
